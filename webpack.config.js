const path = require('path');

module.exports = {
    //mode: 'production',
    mode: 'development',

    entry: {
        traceMapView: './src/traceMapView.ts',
        linepick: './src/linepick.ts',
        stoppick: './src/stoppick.ts'
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    module: {
        rules: [{
                test: /\.ts?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.png$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'public', 'build'),
        sourceMapFilename: "[name].js.map"
    },
    devtool: 'source-map'
};