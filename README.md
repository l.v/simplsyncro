SimplSynchro
===

## Scenarios:
### 1
- select a bus line
- select a bus stop (or geolocalize closest stop)
- select target stop
- view line + time data on map
- bonus: show (pseudo)-real-time buses on the map
- bonus: favorite line / stops
### 2 (bonus)
- pick bus stops via the map (stops directly or closest to picked spot)
### n+1 (bonus)
- geolocalize or pick a spot on the map
- app displays the fastest path to closest toilet

## APIs:
- [Arrêts de bus du réseau de transport en commun Synchro Bus](https://donnees.grandchambery.fr/explore/dataset/t_troncon_shape/)
- [Lignes des réseaux de transport en commun Synchro Bus](https://donnees.grandchambery.fr/explore/dataset/t_troncon_line/information/?disjunctive.nom_ligne)
- [Arrêts et horaires théoriques bus](https://donnees.grandchambery.fr/explore/dataset/arrets-et-horaires-theoriques-bus/information/)

## Tools: 
- openstreetmap
- leaflet
- geolocalisation API
- webpack
- bootstrap

## git orga
- master for working code
- dev for tests
- dev-feature for features
