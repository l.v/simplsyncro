import { LineNameCodeMap } from "./types";

const lines: LineNameCodeMap[] = [
    {
        "lineCode": "L17",
        "lineName": "COLLÈGE H. BORDEAUX / MONZIN"
    },
    {
        "lineCode": "L18",
        "lineName": "FORGERIE / VILLARD-MARIN"
    },
    {
        "lineCode": "L1",
        "lineName": "DE GAULLE / SALINS"
    },
    {
        "lineCode": "L3",
        "lineName": "LES FRASSES / ST-BALDOPH CENTRE"
    },
    {
        "lineCode": "L12",
        "lineName": "LES DÉSERTS - CHAVONNETTES / PARC RELAIS LA TROUSSE"
    },
    {
        "lineCode": "Chrono D",
        "lineName": "CHAMOUX / DEBUSSY / PLAINE DES SPORTS"
    },
    {
        "lineCode": "L16",
        "lineName": "COLLÈGE H. BORDEAUX / LE PONT"
    },
    {
        "lineCode": "Chrono C",
        "lineName": "DE GAULLE / CHALLES CENTRE"
    },
    {
        "lineCode": "Chrono B",
        "lineName": "PARC RELAIS MAISON BRÛLÉE / ROC NOIR"
    },
    {
        "lineCode": "VELOBULLE_2",
        "lineName": "VÉLOBULLE LA RAVOIRE"
    },
    {
        "lineCode": "L10",
        "lineName": "PARC RELAIS MAISON BRÛLÉE / PARC RELAIS MAISON BRÛLÉE"
    },
    {
        "lineCode": "L11",
        "lineName": "PRAGONDRAN / COLLÈGE DE MAISTRE"
    },
    {
        "lineCode": "Chrono A",
        "lineName": "INSEEC / UNIVERSITE JACOB"
    },
    {
        "lineCode": "LFeclaz",
        "lineName": "CHAMBÉRY / LA FÉCLAZ"
    },
    {
        "lineCode": "L5",
        "lineName": "CURIAL / ST-BALDOPH"
    },
    {
        "lineCode": "L19",
        "lineName": "L’ÉVÊQUE / FERRY"
    },
    {
        "lineCode": "L4",
        "lineName": "COLLÈGE G. SAND / CHAMPET"
    },
    {
        "lineCode": "L15",
        "lineName": "MONGE / HAUTS DES LAMETTES"
    },
    {
        "lineCode": "L13",
        "lineName": "COLLÈGE J. MERMOZ / LA GUILLÈRE"
    },
    {
        "lineCode": "VELOBULLE_1",
        "lineName": "VÉLOBULLE CHAMBÉRY"
    },
    {
        "lineCode": "L2",
        "lineName": "LANDIERS NORD - CÉVENNES / GONRAT"
    },
    {
        "lineCode": "L14",
        "lineName": "COLLÈGE J. MERMOZ / NÉCUIDET"
    }
];

export default lines;
