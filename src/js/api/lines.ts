import * as constants from "../constants";
import { ApiTronconLineResponse } from "../types";
import { reverseLinestringCoords } from "../utils";

const url = constants.basePath + 'dataset=t_troncon_line&facet=code_ligne&facet=nom_ligne&facet=commune&rows=50&'

async function doQuery (searchParms: string): Promise<ApiTronconLineResponse> {
    const response : Response = await fetch(url + searchParms);
    const json : ApiTronconLineResponse = await response.json();

    return json;
}

//API code lines "getCodeLineTrace("L1")"
export async function getCodeLineTrace(codeLine: string) {
    
    var searchParms = new URLSearchParams([
         ['refine.code_ligne', codeLine]
    ])
    const linesData = await doQuery(searchParms.toString())
    const linePath = linesData.records[0].fields.geo_shape.coordinates.map(reverseLinestringCoords);

    return linePath;
}

//API name lines "getNameAndlineTrace(DE GAULE / SALINS')"
export async function getNameLineTrace(nameLine: string){

    var searchParms = new URLSearchParams([
         ['refine.nom_ligne', nameLine],

    ])
    const linesData = await doQuery(searchParms.toString())
}
