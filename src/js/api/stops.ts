import * as constants from "../constants";
import { ApiTronconShapeResponse } from "../types";

const url = constants.basePath + 'dataset=t_troncon_shape&rows=500&facet=nom_arret&facet=commune&facet=lignes&'

async function doQuery(searchParms: string): Promise<ApiTronconShapeResponse>{
    const response : Response = await fetch(url + searchParms);
    const json : ApiTronconShapeResponse = await response.json();

    return json;
}

//API Name stops bus "getStopsData('Croix de la Brune')""
export async function getStopsData(stopName: string){

    var searchParms = new URLSearchParams([
        ['refine.nom_arret', stopName]
    ])
    const stopsData = await doQuery(searchParms.toString())

    return stopsData;
} 

export async function searchStopsForLine(StopsforLine: string){
    var searchParms= new URLSearchParams([
        ['q', StopsforLine]
    ])
    const stopsData = await doQuery(searchParms.toString())

    return stopsData;
}
