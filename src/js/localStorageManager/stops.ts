import { getStopsData as apiQuery } from "../api/stops";
import { storageAvailable } from "../utils";
import { ApiTronconShapeResponse } from "../types";

//await apiQuery()
export async function getStopsData(stopName: string): Promise<ApiTronconShapeResponse> {

    if (storageAvailable('localStorage')) {
        const dataInStorage = localStorage.getItem(stopName)
        if (dataInStorage === null) {
            const stopNames = await apiQuery(stopName);
            localStorage.setItem(stopName, JSON.stringify(stopNames));
            return stopNames;  
        }
        else {
            return JSON.parse(dataInStorage)
        }
    }
    else {
        return apiQuery(stopName);
    };
}
