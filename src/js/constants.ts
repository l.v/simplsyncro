import * as types from "./types";
import generated_lines from "./generated_lines";

export const basePath: string = "https://donnees.grandchambery.fr/api/records/1.0/search/?";

export const linesCodeToNameMap: types.LineNameCodeMap[] = generated_lines;
