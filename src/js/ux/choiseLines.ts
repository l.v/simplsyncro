import * as constants from "../constants";
import { LineNameCodeMap } from "../types";

function createLineElem(data: LineNameCodeMap) : string {
  return `        
  <a href="stops.html?code_ligne=${data.lineCode}">
    <article  class="line my-2 bg-dark text-light border rounded-top rounded-bottom border-dark">
      <p>
          ${data.lineName}, ${data.lineCode}
      </p>
  </article>
</a>`;
}

document.addEventListener("DOMContentLoaded", () => {
  constants.linesCodeToNameMap.forEach(lineData => {
    //TODO: sanitize generated html
    document.getElementById('choiceLines').innerHTML += createLineElem(lineData);
  });

  const lineElmCol = document.getElementsByClassName('line')
  const lineElms = Array.from(lineElmCol)
    lineElms.forEach(element => {
      element.addEventListener('click', (e) => {
        const elem = <HTMLElement>e.target;
        elem.classList.remove("bg-dark")
        elem.classList.remove("text-light");
        elem.classList.add("bg-success")
        elem.classList.add("text-dark");
    });
  });
});
